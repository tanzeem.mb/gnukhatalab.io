---
title: "Full Stack Web Developer"
date: 2023-01-10T15:27:20+05:30
draft: true
---

The GNUKhata project is looking for a passionate full stack web developer. Our tech stack includes Vuejs, Bootstrap, Pyramids, PostgreSQL, Docker, Docker Compose.

### About Us

The GNUKhata project aims to cater small & medium businesses in India by providing a free to use and open source accounting web application with GST compliance.

### Candidate Requirements

- The candidate is expected to have a solid understanding of HTML5, CSS, Javascript, Python, Ability to use Git, Exposure to working with front-end MVC frameworks (VueJs preferably), Good understanding of REST API concepts, SQL.

- Willingness to learn new tools on demand.

- A good understanding of free software philosophy is appreciated

- Exposure to financial domain will be an added asset.

### Job Details

- The candidate can choose to work full time or part time.

- This role is fully remote.

### Contact Info

Please send your resume in pdf, plain text or html format to the email careers@gnukhata.in with subject "Web Developer"

<!-- # Application Tester -->
<!---->
<!-- ### About US -->
<!---->
<!-- The GNUKhata project aims to cater small & medium businesses in India by providing a free to use and open source accounting web application with GST compliance. -->
<!---->
<!-- ### Candidate Requirements -->
<!---->
<!-- - The candidate will test the web app for any bugs & report it using gitlab issues -->
<!---->
<!-- ### Job Details -->
<!---->
<!-- - This role is Part time -->
<!-- - This role is fully remote. -->
<!---->
<!-- ### Contact Info -->
<!---->
<!-- Please send your resume in pdf, text or html format to email `careers@gnukhata.in` with subject "Application Tester" -->
