---
title: "Fullstack Developer"
date: 2023-03-29T11:45:49+05:30
draft: false
---

> The GNUKhata project is looking for a passionate full stack web developer. Our tech stack includes Vuejs, Bootstrap, Pyramids, PostgreSQL, Docker, Docker Compose.

### About Us

The GNUKhata project aims to cater small & medium businesses in India by providing a free and open source accounting web application with GST compliance. This project is financially supported by [Accion Labs](https://accionlabs.com/).

### Required qualifications

- Have a good understanding of HTML5, CSS, Javascript, Python, SQL, Git
- Exposure to working with any front-end MVC frameworks (Vue Js preferably)
- Decent understanding of the REST API concepts
- Using the GNU/Linux operating system
- Willingness to learn new tools on demand

### Desirable qualifications

- Exposure to financial domain, Understanding of accounting concepts & Indian GST law
- Knowledge of docker, Gitlab CI/CD pipelines
- A good understanding of the [free software](https://www.gnu.org/philosophy/philosophy) philosophy

### Nature of Work

- You will be involved in all the phases of the SDLC

- Unlike a traditional job, where the candidate will be trained step by
  step, This job will be more suitable for self learners and those who
  have experience with reading existing code and building on top of it with
  less guidance.

- Your responsibilies include requirement gathering, R&D, writing
  features, fixing bugs, shipping code, documentation, updating the website etc. Also you will interact with the GNUKhata community,
  understand the issues they face with the app and provide solutions.

### Job Details

- Candidates are not discriminated on any basis. Skill is the only parameter. We value the [Hacker Culture](https://en.m.wikipedia.org/wiki/Hacker_culture#Ethics_and_principles)

- Can choose to work full time or part time

- This position could be fully work-from-home (WFH) (within India only)

- Annual compensation for the full time role could range from ₹4,00,000 to ₹6,00,000 depending on your skill level

### Contact

Please send your resume in pdf, plain text or html format to the email [gnukhata@gnowledge.org](mailto:gnukhata@gnowledge.org) with subject "Fullstack Developer - < your name>"
