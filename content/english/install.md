---
title: "Installation Instructions"
date: 2022-10-03T23:10:04+05:30
draft: false
---

In order to install & run gnukhata on your computer, You need to follow the below steps:

## 1. Installing docker & docker compose

### Linux

For ubuntu/debian based distributions:

`sudo apt install docker.io docker-compose`

### Windows & Mac

Head over to https://www.docker.com & download the installer & follow the install Instructions

## 2. Setup & Launching the app

Download the docker compose file: [Click to Download](https://gitlab.com/gnukhata/build/-/raw/master/docker-compose.yml?inline=false)

`cd` into the directory where the file is downloaded

Then, run the command
`sudo docker-compose up -d`

Now you can access gkapp (web UI) inside your web browser at url [http://localhost:2020](http://localhost:2020) & gkcore (REST API Server) [http://localhost:6543](http://localhost:6543) respectively

To stop gnukhata, run `sudo docker-compose down` in the same directory.

> In case of issues in the installation process, please join our [support groups](/get-involved) & we will try to help you out

> gnukhata's data is stored in the `gkdir` folder in user's home directory
