---
title: "Weekly Meeting: 06-10-2022"
date: 2022-10-06T15:02:34+05:30
draft: false
---

## Karthik

- update screenshots for the website
- add installation section
- update downlods section

## Survesh

- updated the closing stock in P/L reports
- added support for opening stock value for products

## GN

ABSENT

## VK

- add text stating what kind of algorithm is used for calcualting closing stock in P/L report
- Explore gnukhata plugin for nextclound in future
- if possible, change screenshots of WALLMART to some other company
- add a blog post on project status end ETA for the release

## rest2e2

ABSENT
