---
title: "Weekly Meeting: 25-01-2023"
date: 2023-01-25T23:59:09+05:30
draft: false
---

## Karthik
* Close Books & Roll Over API Issues (WIP)
* Misc bug fixes

## Survesh
* Fix the issues in voucher create page
* Refactor invoice (WIP)

## GN

## VK

## rest2e2
ABSENT

**Discussions on interviews for GNUKhata:**
- Karthik will be point of contact between Accion and the GNUKhata team for the interview.
- The L1 and L2 interviews conducted at Accion will be considered preliminary screenings.
- Based on the results of the screening, GN and VK will take the final call. 