---
title: "Weekly Meeting: 27-04-2022"
date: 2022-04-27T12:00:31+05:30
draft: true
---

## Karthik

- Worked on the dashboard API & UI

## Survesh

### Features

- [x] Recreate the autocomplete component or use one from online
- [x] (#239) GSTR-3B show invoice details same as in GSTR-1 with search and filtering sort

### Bugs

[GSTR1]

- [x] (#241) opening wallmart, GSTR-1, march 2021 HSN details i get the following error
      `JSON.parse: unexpected non-whitespace character after JSON data at line 1 column 2 of the JSON data`
- [x] (#243) wallmart, debit note 10/DN-22 dated 31-3-2022 is made on sales to unregistered person. this is not being reported in GSTR-1 - Not a bug, value less than 2,50,000
      [DRCR]
- [x] In invoices with more than one item, the items after first one are displayed with 0 rate

## GN

- Generic term for customer/supplier
- use case for non profit org
- regarding gitlab's opensource application program, we will update in a week

## VK

Absent

## rest2e2

- Checkout the tally gst dashboard for reference
- in GST-R1 CDNR for local states must be represented in B2CS with negative value
