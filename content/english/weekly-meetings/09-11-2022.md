---
title: "Weekly Meeting: 09-11-2022"
date: 2022-11-09T12:13:52+05:30
draft: false
---

## Karthik

- applied updates to gkapp based on comments from previous Meeting
- refactored the registers ui

## Survesh

- added api docs to the gkcore wiki

## GN

ABSENT

## Discussion

> Survey that has to be presented to the users regarding OS preference, data migrations etc.

VK: do not send survey link directly in social media handles, Instead create a blog post and link the survey there.

- update the sequence of the Survey
- add a disclaimer that we do not intent to develop data migration tool & this survey helps us to redefine our priority
- add questions like
  - how many years of data you have on gnukhata?
  - what new features you will like in gnukhata, which you feel more useful?

R2: add an extra question: would you like cloud based or self hosted service in future?

> Regarding making an user manual

VK: Regarding user manual, Inform user that it might likely to be taken up soon after the launch of stable version.

Survesh, Karthik: We have to go through the old version & check if it needs a re write or we could refactor the existing one.
