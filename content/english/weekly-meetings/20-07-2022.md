---
title: "Weekly Meeting: 20-07-2022"
date: 2022-07-20T13:11:18+05:30
draft: false
---

## Karthik
[gkcore]
1. json import returns duplicate entry list category wise in response 
2. able to get logs between date range fromdate & todate 
3. only admin can see org wide logs
(absent)

## Survesh
- (WIP) Decoupling Users and Organisations tables
- Check if the public sandbox API's are usable and testable
- Check with ERPNext team on how they handle GST compliance
- Check with Abhijith if Onlinekhata code is open sourced

## GN
(absent)

## VK
- Ash suggested to make GNUKhata compliant with existing GST sandbox API's, before going to a GSP
- Check if online khata code is open source and if it is use their code for the decoupling feature
- Check out the conference https://indiafoss.net/, one of the main sponsores is frappe and we can possibly make some connections if we attend.

## rest2e2
- Onlinekhata has a good login flow where users and organisations are decoupled. (https://www.youtube.com/watch?v=_c6B0WfBsmk&t=338s)
- We can check if their code is open source and if it is we can use that as its what we are trying to achieve


